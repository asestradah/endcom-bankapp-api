const Account = require('../models/Account');
const fetch=require('node-fetch');

//web services del PDF
const URL_WEB_SERVICE= 'https://60005b36cb21e10017af8d5b.mockapi.io/api/v1/account';

//API personal de pruebas
//const URL_WEB_SERVICE= 'https://60039108a3c5f10017913492.mockapi.io/cuenta';

//Realiza una peticion POST de la web service
async function fetchRequest(data){
    const response= await fetch(URL_WEB_SERVICE,{
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
    });
    const res=await response.json();
    return res;
}

//Crea una cuenta a partir de una peticion de web service
function create (req, res) {
    try{
        let nameReq=req.body.name;
        let mailReq=req.body.mail;
        var account= new Account();
    
        if(nameReq&&mailReq){
            let info={
                name:nameReq,
                mail:mailReq
            };
                fetchRequest(info).then(data=>{
                    var idTemp=String(data.id).padStart(3,0);
                    var accountTemp=String(data.account);
                    var accountOK=accountTemp.concat(idTemp);
    
                    account.name=nameReq;
                    account.mail=mailReq;
                    account.account=accountOK;
                    
                    account.save((err)=>{
                        if(err) return res.status(500).send({message: `Error al crear la cuenta: ${err}`})
                        return res.status(200).send({
                            statusCode: 200,
                            message: 'Cuenta creada',
                            id: account._id,
                            account: account.account,
                            balance: parseFloat(account.balance),
                            name: account.name,
                            mail: account.mail
                        })
                    });
                });        
        }
        else if(!nameReq||!mailReq){
            return res.status(500).send({message: 'No es posible continuar sin los datos correctos'})
        }
    }catch(error){
        return res.status(500).send({message: 'Error en el servidor'})
    }
}

//Actualiza la informacion del balance de una cuenta a partir del numero de cuenta
function update (req,res){
    let accountToUpdate=req.body.account;
    let newBalance=req.body.balance;

    Account.findOneAndUpdate({account:accountToUpdate},{balance:newBalance},(err,accountData)=>{
        if(err) return res.status(500).send({message: `Error al consultar la cuenta: ${err}`})
        if(!accountData) return res.status(500).send({message: 'La cuenta no existe'})
        return res.status(200).send({
                statusCode: 200,
                message: 'Saldo actualizado',
                account: accountData.account,
                balance: parseFloat(newBalance)
        });
    });
}

//Muestra la informacion de una cuenta buscandola por el numero de cuenta
function show(req,res){
    let data=req.params.account;
    Account.findOne({account:data},(err,accountData)=>{
       
        if(err) return res.status(500).send({message: `Error al consultar la cuenta: ${err}`})
        if(!accountData) return res.status(500).send({message: 'La cuenta no existe'})
        return res.status(200).send({
            statusCode: 200,
            message: 'Informacion de cuenta',
            id: accountData._id,
            account: accountData.account,
            balance: parseFloat(accountData.balance),
            name: accountData.name,
            mail: accountData.mail
        })
    });
}

module.exports ={
    create,
    update,
    show
}