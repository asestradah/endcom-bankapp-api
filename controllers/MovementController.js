const Movement = require('../models/Movement');
const Account = require('../models/Account');
const fetch=require('node-fetch');

//Web services del pdf
//const URL_WEB_SERVICE= 'https://60005b36cb21e10017af8d5b.mockapi.io/api/v1/account';

//API personal de pruebas
const URL_WEB_SERVICE= 'https://60039108a3c5f10017913492.mockapi.io/catalog';

//Funcion para obtener datos de los web services
async function fetchRequest(){
    const response= await fetch(URL_WEB_SERVICE,{
        method: 'GET'
    });
    const res=await response.json();
    return res;
}

//Crea una cuenta a partir de una peticion de web service
function create (req, res) {
    try{
        let accountReq=req.body.account;
        let amountReq=req.body.amount;
        let descReq=req.body.description;
        var movement= new Movement();
    
        if(accountReq&&amountReq&&descReq){
            let info={
                account: accountReq,
                amount: amountReq,
                description: descReq
            };
            Account.findOne({account:info.account},(err,accountData)=>{
                if(err) return res.status(500).send({message: `Error al consultar la cuenta: ${err}`})
                if(!accountData) return res.status(500).send({message: 'La cuenta no existe'})

                let actualBalance=accountData.balance;
                if(actualBalance>=info.amount){
                    let newBalance=actualBalance-info.amount;
                    movement.account=info.account;
                    movement.description=info.description;
                    movement.amount=info.amount;
                    movement.save((err)=>{
                            if(err) return res.status(500).send({message: `Error al crear el movimiento: ${err}`})
                            Account.findOneAndUpdate({account:accountData.account},{balance:newBalance},(e)=>{
                                if(e) return res.status(500).send({message: `Error al actualizar el saldo: ${err}`})
                            });
                            return res.status(200).send({
                                statusCode: 200,
                                message: 'Movimiento registrado'
                            })

                        });
                        
                }else{
                    return res.status(400).send({
                        statusCode: 400,
                        message: 'No cuentas con saldo suficiente'
                    });
                }
                    
                        
            });
        }else if(!accountReq||!amountReq){
            return res.status(500).send({message: 'No es posible continuar sin los datos correctos'})
        }
    }catch(error){
        return res.status(500).send({message: `Error al procesar los datos: ${error}`})
    }
}

//Funcion que actualiza el balance de una cuenta a partir del monto que se le inserte en el body
function update (req,res){
    let idMovement=req.body.id;
    let movementCode=req.body.movementCode;
    fetchRequest().then(data=>{
        var flag=false;
        for (var i in data) {
            val = data[i];
            if(movementCode==String(val.code)){
                flag=true;
                Movement.findByIdAndUpdate(idMovement,{movementCode:val.code},(error)=>{
                    if(error) return res.status(500).send({message: `Error al crear el movimiento: ${error}`})
                    return res.status(200).send({
                        statusCode: 200,
                        message: 'Movimiento actualizado'
                    })
                });
                break;
            }
        }
        if(!flag) return res.status(400).send({
            statusCode: 400,
            message: 'El tipo de movimiento no existe'
        })
    });
}

//Funcion para mostrar los movimientos de una cuenta en especifico
function show(req,res){
    let accountFind=req.params.account;

    Account.findOne({account:accountFind},(err,accounts)=>{
        if(err) return res.status(500).send({message: `Error al buscar la cuenta: ${err}`})
            var json={
                statusCode:200,
                message:"Movimientos de cuenta",
                account:accounts.account,
                movements: []
            }
            var data=JSON.parse(JSON.stringify(json));
            Movement.find({account:accounts.account},(error,movements)=>{
                if(error) return res.status(500).send({message: `Error al buscar el movimiento: ${error}`})
                var array=new Array();
                var array2=new Array();

                //OPCIONAL: Se agregan los datos de la web services en la consulta
                fetchRequest().then((dataWeb,err)=>{
                    if(err) return res.status(500).send({message: `Error al obtener datos del servicio: ${err}`})
                    for (var i in movements) {
                        val = movements[i];
                        for (var i in dataWeb) {
                            value = dataWeb[i];
                            if(val.movementCode==String(value.code)){
                                array2.push({
                                    movementName:value.name
                                })
                                break;
                            }
                        }
                    }
                    for (var i in movements) {
                        val = movements[i];
                        array.push({
                            id:val._id,
                            movementCode:val.movementCode,
                            description:val.description,
                            movementName:array2[i].movementName,
                            amount:val.amount,
                            createDate:val.createDate
                        })
                    }
                    data.movements=array;
                    return res.status(200).send({data})
                });
            })        
    })
}

module.exports={
    create,
    update,
    show
}