const config={
    appConfig:{
        port: process.env.APP_PORT || 3050,
    },
    db: {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || 27017,
        dbName: process.env.DB_NAME || 'endcome-api-db'
    }
}

module.exports=config;