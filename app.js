'use strict'

//Dependencias
const express = require('express');
const routes = require('./routes')
const {appConfig,db}=require('./config/config');
const connectDB=require('./config/database');
const app = express();

//Encabezados CORS
app.use(function(req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//indicar uso de rutas
app.use(routes);

//Conexion a base de datos
connectDB(db);

// Iniciar servidor
const server = app.listen(appConfig.port, (error) => {
    if (error) return console.log(`Error al iniciar el servicio: ${error}`);

    console.log(`Server en puerto ${server.address().port}`);
});