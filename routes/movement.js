const  {Router} = require('express');
const movementRouter = Router();
const MovementController= require('../controllers/MovementController');

//Rutas

//Crear movimiento
// rutaprincipalAPI/account/movement
movementRouter.post('', MovementController.create)

//Actualizar movimiento
// rutaprincipalAPI/account/movement
movementRouter.put('', MovementController.update)

//Consultar movimientos
// rutaprincipalAPI/account/movement/:account
movementRouter.get('/:account', MovementController.show);


module.exports = movementRouter;