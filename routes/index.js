const {Router} = require('express');
const accountRouter = require('./account');
const router = Router();
const bodyParser = require('body-parser');

const AccountController= require('../controllers/AccountController');

//Middleware
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: false,
}));

//Mensaje inicial de Api
router.get('/', (req, res) => {
    res.send({
        message: 'API REST funcionando correctamente'
    });
});

//No se usa favico
router.get('/favico.ico', (req, res) => {
    res.sendStatus(204);
});

//Solo esta ruta de cuenta al inicio por el nombre de la ruta
router.post('/create-account', AccountController.create);
//Se usa rutas de cuentas
router.use('/account', accountRouter);


module.exports = router;