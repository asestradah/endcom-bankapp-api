const  {Router} = require('express');
const accountRouter = Router();
const AccountController= require('../controllers/AccountController');

const movementRouter = require('./movement');

//Rutas
// rutaprincipalAPI/account
accountRouter.put('', AccountController.update)

// rutaprincipalAPI/account/:account
accountRouter.get('/:account', AccountController.show);

//Rutas de movimientos
accountRouter.use('/movement',movementRouter);

module.exports = accountRouter;