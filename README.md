# API REST endcom-bankapp-api

Esta Api funciona con 2 modelos de datos, incluyendo 2 Web Services

**IMPORTANTE**

* La web service para las peticiones de Account funciona correctamente
* La web service para las peticiones de Movements no funciona de manera correcta, por lo que se decidio crear una API alojada en mockapi para fines de pruebas. Si se desea cambiar la URL del web service solo se tiene que modificar desde el archivo MovementController.

![Cabecera del codigo MovementController.js](/assets/c1.png "Captura de pantalla")

* Es necesario tener instalado la siguiente paqueteria:

1. Node js 14.15.4 o similar
2. MongoDB 4.4.3 o similar

* En la carpeta **Assets** de este repositorio se incluye la coleccion de peticiones para que pueda realizar sus pruebas usando Insomia


## INSTALACION DEL PROYECTO

1. En una terminal ubica la carpeta donde quiere alojar el proyecto con el comando `cd`

2. Inserte la siguiente linea de código y presione enter

`git clone https://bitbucket.org/asestradah/endcom-bankapp-api.git`

3. Para tener todas las dependencias necesarias ejecute el siguiente comando

`npm install --save express body-parser node-fetch mongoose`

4. Para poder usar el proyecto de forma local se recomienda instalar nodemon

`npm install -D nodemon`

* Adicional a la instalacion, debe configurar el archivo package.json para poder ejecutar el servidor
* Abre el archivo package.json y busca el apartado *scripts*. Arriba de *test* insertamos:

`"start": "nodemon app.js",`

![Archivo package.json](/assets/c2.png "Captura de pantalla")

El servidor por defecto usa el puerto 3050 para express y 27017 para MongoDB, si desea cambiar alguno de estos puertos puede dirigirse al archivo *config/config.js* y modificarlos a su necesidad

## EJECUCION DE SERVIDOR

Estando dentro de la ubicacion del proyecto en una terminal ejecute el comando:

`npm start`

Si necesita modificar el codigo no es necesario reiniciar el servidor, automaticamente se guardan los cambios que haga al codigo.

Cabe mencionar para que la API funcione correctamente es necesario que este ejecutandose el servicio *mondod* en su computadora.

## PETICIONES GET, POST, PUT

### Peticiones GET

#### Obtener cuenta
Se obtiene la informacion relacionada con una sola cuenta.

Solo es necesario que al final de la URL se agregue el **Número de cuenta**

`http://localhost:3050/account/ **CUENTA**`

#### Obtener movimientos
Se obtiene la informacion de todos los movimientos relacionada a una sola cuenta.

Solo es necesario que al final de la URL se agregue el **Número de cuenta**

`http://localhost:3050/account/movement/ **CUENTA**`

### Peticiones POST

#### Crear cuenta

`http://localhost:3050/create-account` 

Se crea una nueva cuenta.
Es necesario mandar un objeto JSON con el **Nombre completo del usuario** y el **Correo electronico**
Ejemplo:

```
{
 "name": "Nombre completo",
 "mail": "mail@endcom.mx"
}
```

#### Crear movimiento

`http://localhost:3050/account/movement`

Se crea un nuevo movimiento.
Es necesario mandar un objeto JSON con el **Número de cuenta**, el **Monto del movimiento** y la **Descripcion**
Ejemplo:

```
{
 "account": "86646692001",
 "amount": 50,
 "description": "mail@endcom.mx"
}
```

### Peticiones PUT

#### Actualizar saldo de cuenta

`http://localhost:3050/account`

Se actualiza el saldo de una cuenta existente.
Es necesario mandar un objeto JSON con el **Número de cuenta** y el **Nuevo saldo**
Ejemplo:

```
{
 "account": "86646692001",
 "balance": 500
}
```

#### Actualizar codigo de movimiento

`http://localhost:3050/account/movement`

Se actualiza el codigo de un movimiento existente.
Es necesario mandar un objeto JSON con el **ID del movimiento** y el **Nuevo código de movimiento**
Ejemplo:

```
{
 "id": "663827jh2948eea43",
 "movementCode": "M002",
}
```