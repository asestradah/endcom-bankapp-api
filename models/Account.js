const mongoose=require('mongoose');
const Movement = require('./Movement');

const AccountSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        unique: [true,'El id ya existe']
    },
    name: {
        type: String,
        required: [true,'El nombre es requerido'],
        minlength: [3,"Se necesitan al menos 3 caracteres"]
    },
    mail: {
        type: String,
        required: [true,'El correo es requerido'],
        match: [/.+\@.+\..+/, 'Por favor ingrese un correo válido']
    },
    account: {
        type: String,
        required: [true,'Se necesita un código de cuenta'],
        maxlength: [11,"Solo se permiten 11 caracteres"]
    },
    balance: {
        type: Number,
        default:1000
    }
});

const Account = mongoose.model('Account',AccountSchema);
module.exports=Account;