const mongoose=require('mongoose');
const Account = require('./Account');

const MovementSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.Types.ObjectId,
        unique: true
    },
    movementCode: {
        type: String,
        required: true,
        maxlength: [4,"Solo se permiten 4 caracteres"],
        default: "M001"
    },
    description:{
        type: String,
        default: "",
        maxlength: [4,"Solo se permiten 4 caracteres"],
    },
    amount:{
        type: Number,
        required: true,
        validate:{
            validator: function(v){
                return v!=0;
            },
            message: 'No puede ser cero'
        }
    },
    account:{
        type: String, 
        required: true,
        maxlength: [11,"Solo se permiten 11 caracteres"],

    },
    createDate:{
        type: Date,
        default: Date.now,
        required: true
    }
});

const Movement = mongoose.model('Movement',MovementSchema);
module.exports=Movement;